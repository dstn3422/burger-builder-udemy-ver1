import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://my-burger-545f6-default-rtdb.firebaseio.com/',
});

export default instance;
